//
// Copyright 2018  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package routes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

// GetDeviceTarFile : Get Device Tar File
func GetDeviceTarFile(w http.ResponseWriter, r *http.Request) {
	log.Println("Inside GET /cgi-bin/pvrlocal/{revision} Handler")
	params := mux.Vars(r)
	revision := params["revision"]
	deviceID := "5d82280b944a53000ab0ea2c"
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	filename := wd + "/storage/sideload/device.gz"
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	fi, err := f.Stat()
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Disposition", "attachment; filename="+deviceID+"_"+revision+".tar.gz")
	http.ServeContent(w, r, "device.tar.gz", fi.ModTime(), f)
}

// PostDeviceTarFile : Post Device Tar File
func PostDeviceTarFile(w http.ResponseWriter, r *http.Request) {
	log.Println("Inside POST /cgi-bin/pvrlocal Handler")
	w.Header().Set("Content-Type", "application/json")

	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	fileBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err)
	}
	filename := "device.gz"
	log.Print("handler.Filename:" + filename)
	err = ioutil.WriteFile(wd+"/storage/sideload/"+filename, fileBytes, 0777)
	if err != nil {
		log.Fatal(err)
	}
	w.WriteHeader(http.StatusOK)
	response := map[string]interface{}{
		"status":  1,
		"message": "Posted Successfully",
	}
	json.NewEncoder(w).Encode(response)
}
