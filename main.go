//
// Copyright 2018  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package main

import (
	"log"
	"net/http"

	"gitlab.com/pantacor/cgi-api/routes"

	"github.com/gorilla/mux"
)

func main() {

	router := mux.NewRouter()

	/* will produce a tarball in pvr sdk format using
	 current revision.Will name the file in content-disposition
	header like the device id + REVISION.
	 it will then stream the file to client */
	router.HandleFunc("/cgi-bin/pvrlocal/{revision}", routes.GetDeviceTarFile).Methods("GET")
	/*
	 POST /cgi-bin/pvrlocal will accept a application/octet-stream
	 encoded binary stream
	*/
	router.HandleFunc("/cgi-bin/pvrlocal", routes.PostDeviceTarFile).Methods("POST")
	log.Fatal(http.ListenAndServe(":2005", router))
}
